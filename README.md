Django Boilerplate Technical Doc & Flow
===

This documentation will describe technologies and tools uesed to develop this application.
Besides this documentation also provide the procedure for running the application.

##### Language:
    Python (3.6.0)
     
##### Database:
    MongoDB

##### Frameworks:
    Django (3.2.6)
    django rest framework (3.12.4)

##### Other libraries / tools:
    pymongo (3.12.1)
    djongo (1.3.6)
    Pillow (8.3.1)
    django-oauth-toolkit (1.5.0)
    django-sendgrid-v5 (1.2.0)
    Others can be found in requirement.txt file


<br>

#### Project Description:

It's Boilerplate with authentication service.



#### Project Structure:

All modules are django app based. Following tree will represent the project structure. <br>
Note: In the tree, root folder files and folders will be labeled. `django_boiler` folder is the project setting folder and
`serviceapp` is the standard django app folder structure.   


```
├── .gitignore (git ignore file)
├── manage.py (project entrypoint)
├── README.md (technical doc & flow file)
├── requirements.txt (project dependencies)
├── django_boiler
│   └── settings.py (project main setting and config)
│   └── urls.py (project main routing)
│   └── wsgi.py (standard for web server config)
│   └── ...
├── serviceapp
│   └── migrations
│      └── 001_initial.py (app migration file)
│      └── ...
│   └── serializers
│      └── user_serializer.py (user sercialzer config)
│      └── ...
│   └── templates (template files)
│      └── ...
│   └── views (app url's functionality)
│      └── ...
│   └── admin.py (app admin config)
│   └── apps.py (app setting file)
│   └── models.py (app models are here)
│   └── urls.py (app routing)
│   └── ...
└── ...
```

## Tests

To run the tests, `cd` into the directory where `manage.py` is:
```sh
(env)$ python manage.py test
