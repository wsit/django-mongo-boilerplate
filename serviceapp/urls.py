from rest_framework.routers import SimpleRouter
from django.conf.urls import url, include
from .views.users import UserViewSet, UserInfo, ResetPasswordRequestViewSet, ResetPasswordView

router = SimpleRouter()
router.register(r'users', UserViewSet, basename='user')

urlpatterns = [
    url(r'^auth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^user-profile/', UserInfo.as_view()),
    url(r'^upload-avatar/', UserInfo.upload_avatar),
    url(r'^forget-password/', ResetPasswordRequestViewSet.forget_password),
    url(r'^reset-password/', ResetPasswordView.as_view()),
    url(r'^email-verification/', ResetPasswordRequestViewSet.email_verification),
    url(r'^change-user-password/', ResetPasswordRequestViewSet.change_user_password),

] + router.urls

