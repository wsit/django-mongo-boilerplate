import hashlib
import random
import string
import pytz

from rest_framework.permissions import BasePermission
from rest_framework import viewsets, status, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from django.conf import settings
from serviceapp.models import Users, ResetPassword
from serviceapp.serializers.user_serializer import UserSerializer
from serviceapp.views.helper import LogHelper
from datetime import datetime, timedelta, date
from serviceapp.views.common import CommonView
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.decorators import api_view
from django.contrib.auth.decorators import login_required
from django.db import transaction


class UserProfilePermissions(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        return False


class UserUploadPermissions(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        elif request.method == 'POST':
            return True
        return False


class AdminPermissions(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        return False


class UserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    create:
    Create a new user instance.
    """
    queryset = Users.objects.all()
    serializer_class = UserSerializer
    permission_classes = (UserUploadPermissions,)

    def create(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                serializer = UserSerializer(data=request.data)
                if serializer.is_valid():
                    obj = serializer.save()
                    key = ''.join(
                        random.choice(string.ascii_letters + string.digits + string.ascii_letters) for _ in
                        range(10)) + str(datetime.now())
                    key = key.encode('utf-8')
                    verification_token = hashlib.sha224(key).hexdigest()
                    obj.email_verification_token = verification_token
                    obj.save()
                    mail_template = "mails/registration_confirmation.html"
                    context = {
                        'key': verification_token
                    }
                    subject = settings.SITE_NAME + " ::Confirm Registration"
                    to = obj.email
                    CommonView.send_email(request, mail_template, context, subject, to)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            LogHelper.efail(e)
            return Response({'status': False, 'message': "Something went wrong."},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserInfo(APIView):
    permission_classes = (UserProfilePermissions, )

    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs ):
        try:
            Users.objects.filter(id=request.user.id).update(**request.data)
            user = Users.objects.get(id=request.user.id)
            serializer = UserSerializer(user)
            new_serializer_data = dict(serializer.data)
            new_serializer_data['avatar'] = CommonView.get_file_path(new_serializer_data['avatar'])
            new_serializer_data['avatar_thumb'] = CommonView.get_file_path(new_serializer_data['avatar_thumb'])
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            LogHelper.efail(e)
            response = {
                "message": "Something went wrong. please try again"
            }
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @api_view(["post"])
    @login_required()
    def upload_avatar(request):
        try:
            updated_data = {}
            if 'avatar' in request.FILES:
                avatar = request.FILES['avatar']
                avatar_info = CommonView.handle_uploaded_file(avatar, request.user)
                if 'path' in avatar_info:
                    updated_data['avatar'] = avatar_info['path']
                    updated_data['avatar_thumb'] = avatar_info['thumb_path']
                    Users.objects.filter(id=request.user.id).update(**updated_data)
                    return Response({'status': True, 'message': "avatar upload successfully."}, status=status.HTTP_200_OK)
            return Response({'status': False, 'message': "Please attach avatar file."}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            LogHelper.efail(e)
            response = {
                "success": False,
                "message": "Something went wrong. please try again"
            }
            return Response(data=response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ResetPasswordRequestViewSet:

    @api_view(["post"])
    def forget_password(request):
        try:
            response = {}
            email = request.data.pop("email", '')
            users = Users.objects.filter(email=email, is_active=1)
            if users.exists():
                user = users[0]
                current_time = datetime.now()
                expired_date = current_time + timedelta(hours=1)
                reset_code = user.resetpassword_set.filter(already_used=0, expired_at__gt=current_time)
                if reset_code.exists():
                    hash_code = reset_code[0].hash_code
                    ResetPassword.objects.filter(id=reset_code[0].id).update(expired_at=expired_date)
                else:
                    # generate hash code and store
                    key = ''.join(
                        random.choice(string.ascii_letters + string.digits + string.ascii_letters) for _ in
                        range(10)) + str(datetime.now())
                    key = key.encode('utf-8')
                    hash_code = hashlib.sha224(key).hexdigest()
                    ResetPassword(user=user, hash_code=hash_code, expired_at=expired_date).save()
                mail_template = "mails/reset_password.html"
                context = {
                    'key': hash_code
                }
                subject = settings.SITE_NAME + " ::Password Reset"
                to = user.email
                CommonView.send_email(request, mail_template, context, subject, to)
                response['success'] = True
                response['message'] = "A reset password email is sent to you with confirmation link"
                return Response(response, status=status.HTTP_200_OK)
            else:
                return Response({'success': False, 'message': "Email doesn't found"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            LogHelper.efail(e)
            return Response({'success': False, 'message': "Something went wrong."},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @api_view(["post"])
    def change_user_password(request):
        try:
            if not request.user.is_authenticated:
                return Response({'success': False, 'message': "User not authorized."},
                                status=status.HTTP_401_UNAUTHORIZED)
            old_password = request.data.pop("old_password", '')
            password = request.data.pop("password", '')
            confirm_password = request.data.pop("confirm_password", '')
            if len(password) < 6:
                return Response({"password": "Password should be minimum 6 characters"}, status=status.HTTP_400_BAD_REQUEST)
            elif password != confirm_password:
                return Response({"password": "Passwords did not match"}, status=status.HTTP_400_BAD_REQUEST)
            if request.user.check_password(old_password):
                request.user.set_password(password)
                request.user.save()
            else:
                return Response({"old_password": "Old Passwords did not match"}, status=status.HTTP_400_BAD_REQUEST)
            return Response({'success': True, 'message': "Password Change Successfully."}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'success': False, 'message': "Something went wrong."},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @api_view(["get"])
    def email_verification(request):
        response = {}
        try:
            if request.user.is_authenticated:
                response['success'] = True
                response['message'] = "Already logged in"
                return Response(response, status=status.HTTP_200_OK)
            verification_code = request.GET["key"]
            user = Users.objects.get(email_verification_token=verification_code)
            if user.email_expired_at > date.today():
                user.is_active = True
                user.save()
                response['success'] = True
                response['message'] = "Email verified"
                return Response(response, status=status.HTTP_200_OK)
            else:
                response['success'] = False
                response['message'] = "Email authentication expired"
                return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except ObjectDoesNotExist:
            response['success'] = False
            response['message'] = "Invaild token"
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            LogHelper.efail(e)
            response['success'] = False
            response['message'] = "Something went wrong. Please try again"
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ResetPasswordView(APIView):
    def get(self, request, *args, **kwargs):
        hash_code = request.GET.get('key')
        response = {}
        try:
            reset_password = ResetPassword.objects.get(hash_code=hash_code)
            utc = pytz.UTC
            time_now = datetime.now().replace(tzinfo=utc)
            expired_at = reset_password.expired_at.replace(tzinfo=utc)
            if time_now > expired_at:
                raise PasswordResetException("expired")
            elif reset_password.already_used:
                raise PasswordResetException("used")
            else:
                user = reset_password.user
                response['success'] = True
                response['message'] = "Reset key is ok"
                response['hash_code'] = hash_code
                return Response(response, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            response['success'] = False
            response['message'] = "Not Found"
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        except PasswordResetException as e:
            if e.message == 'expired':
                response['success'] = False
                response['message'] = "The link is already expired."
            elif e.message == 'used':
                response['success'] = False
                response['message'] = "The link is already used once by you."
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            LogHelper.efail(e)
            response['success'] = False
            response['message'] = "Something went wrong. Please try again"
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        response = {}
        try:
            hash_code = request.data.pop("key", '')
            password = request.data.pop('password', '')
            confirm_password = request.data.pop('confirm_password', '')
            reset_password = ResetPassword.objects.get(hash_code=hash_code)
            user = reset_password.user
            utc = pytz.UTC
            time_now = datetime.now().replace(tzinfo=utc)
            expired_at = reset_password.expired_at.replace(tzinfo=utc)
            if time_now > expired_at:
                raise PasswordResetException("expired")
            elif reset_password.already_used:
                raise PasswordResetException("used")
            elif len(password) < 6:
                raise PasswordResetException("password_length")
            elif password != confirm_password:
                raise PasswordResetException("password_match")
            elif password == confirm_password:
                user.password = make_password(password)
                user.save()
                reset_password.already_used = True
                reset_password.save()
                response['success'] = True
                response['message'] = "Your password is changed successfully. You can now login"
                return Response(response, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            response['success'] = False
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        except PasswordResetException as e:
            if e.message == 'expired':
                response['success'] = False
                response['message'] = "The link is already expired."
            elif e.message == 'used':
                response['success'] = False
                response['message'] = "The link is already used once by you."
            elif e.message == 'password_length':
                response['success'] = False
                response['message'] = "Password should be minimum 6 characters"
            elif e.message == 'password_match':
                response['success'] = False
                response['message'] = "Passwords did not match"
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            LogHelper.efail(e)
            response['success'] = False
            response['message'] = "Something went wrong. Please try again"
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PasswordResetException(Exception):
    def __init__(self, message):
        self.message = message



